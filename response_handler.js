module.exports.responseHandler = (httpStatus, records, message, error = false) => {
   if(error){
    return {
        code: httpStatus,
        message: message,
        errors: Array.isArray(records) ? records.map(o => o.error ) : records    
    }
   }
   else{
    return {
        code: httpStatus,
        message: message,
        records: Array.isArray(records) ? records.map(o => o ) : records    
    }
   }
}