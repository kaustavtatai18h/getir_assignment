const Joi = require('joi')


const schema = Joi.object({
    startDate : Joi.date().iso().required(),
    endDate : Joi.date().iso().required(),
    minCount : Joi.number().integer().greater(0).required(),
    maxCount : Joi.number().integer().greater(Joi.ref('minCount')).required(),
})


module.exports.validatePayload =  (payload) => {
    return  schema.validate(payload)
}