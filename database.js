require('dotenv').config()
const mongoose = require('mongoose');
const { Schema } = mongoose;


const recordSchema = new Schema({
    key:  String, // String is shorthand for {type: String}
    value: String,
    createdAt: { type: Date},
    counts: Array
  });

const Records = mongoose.model('Records', recordSchema);

main().catch(err => console.log(err));

async function main() {
  await mongoose.connect(process.env.MONGO_URL);
}

module.exports = {Records}