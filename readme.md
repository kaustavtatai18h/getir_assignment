For installing the application:
`npm install`

Setup the Environment:
1) Rename the `.env.example` to `.env`.
2) Add your MongoDB URI to `MONGO_URL`.

For running the application:
1) For development run `npm run dev`.
2) For production run `npm run start`.