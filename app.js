const express = require("express");
const { validatePayload } = require("./request_validator");
const { responseHandler } = require("./response_handler");
const bodyParser = require("body-parser");
const { Records } = require("./database");

const app = express();

// parse application/json
app.use(bodyParser.json());

app.get("/", (req, res, next) => {
  return res.send("Ok! successfull..!!!");
});

app.post("/content", async (req, res) => {
  //validate incoming request body
  let validator = validatePayload(req.body);

  //return the validation error
  if (validator.error)
    return res.send(responseHandler(422, validator, "Error!", true));

  try {
    //fetching data from database
    let records = await Records.aggregate([
      {
        $project: {
          sum: { $sum: "$counts" },
          key:1,
          value:1,
          createdAt:1
        },
      },
      {
          $match: {
              sum: {$gte:parseInt(req.body.minCount),$lte:parseInt(req.body.maxCount)},
              createdAt: {$gte: new Date(req.body.startDate), $lte: new Date(req.body.endDate)},
          }
      }
    ]);
   
    return res.send(responseHandler(200, records, "Success"));
  } catch (e) {
    return res.send(responseHandler(400, "Error!", e.message, true));
  }
});

app.listen(process.env.PORT || 3000, () => {
  console.log("app listening on port 3000.");
});
